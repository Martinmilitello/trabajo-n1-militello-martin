<?php

  require 'database.php';

  $message = '';

  if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $sql = "INSERT INTO users (Nombre, Apellido,Dni, email, password) VALUES (:Nombre, :Apellido,:Dni,:email, :password)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':Nombre', $_POST['Nombre']);
    $stmt->bindParam(':Apellido', $_POST['Apellido']);
    $stmt->bindParam(':Dni', $_POST['Dni']);
    $stmt->bindParam(':email', $_POST['email']);
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $stmt->bindParam(':password', $password);

    if ($stmt->execute()) {
      $message = 'Se ha creado correctamente tu usuario';
    } else {
      $message = 'No se ha podido crear correctamente su usuario';
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Registrarse</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="assets/style.css">
  </head>
  <body>

    <?php require 'partials/header.php' ?>

    <?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>

    <h1>Registrarse</h1>
    <span>o <a href="login.php">Iniciar sesion</a></span>

    <form action="signup.php" method="POST">
      <input name="Nombre" type="text" placeholder="Ingrese su nombre" required="">
      <input name="Apellido" type="text" placeholder="Ingrese su apellido" required="">
      <input name="Dni" type="text" placeholder="Ingrese su Doc. de identidad" required="">
      <input name="email" type="text" placeholder="Ingrese su Email" required="">
      <input name="password" type="password" placeholder="Ingrese su contrasena"required="">
      <input name="confirm_password" type="password" placeholder="Confirme su contrasena"required="">
      <input type="submit" value="Enviar">
    </form>

  </body>
</html>
