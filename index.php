<?php
  session_start();

  require 'database.php';

  if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,Nombre, Apellido, Dni, email, password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $user = null;

    if (count($results) > 0) {
      $user = $results;
    }
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-10">
    <title>Bienvenido a Inmobiliaria SA</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link meta="viewport" content="width=device-width, user-scrambler=no, initial-scale=1.0 , maximum-scale=1.0, minimum-scale=1.0">
   
    <link rel="stylesheet" href="assets/style.css">
    
  </head>
  <body>
    <?php require 'partials/header.php' ?>

    <?php if(!empty($user)): ?>
      <br>Bienvenido.<?= $user['email'] ?>
      <br>Te has registrado correctamente.
      <a href="logout.php">Cerrar sesion</a>
    <?php else: ?>
      <h1>Registrarse o Iniciar sesion</h1>

      <a href="signup.php">Registrarse</a> o
      <a href="login.php">Iniciar sesion</a> 
     

    <?php endif; ?>
  </body>
</html>
