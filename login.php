<?php
 
 session_start();

    if (isset($_SESSION['user_id'])) {
      header('Location: /php-login');
    }
    require 'database.php';

    if (!empty($_POST['email']) && !empty($_POST['password'])) {
      $records = $conn->prepare('SELECT id, email, password FROM users WHERE email = :email');
      $records->bindParam('email', $_POST['email']);
      $records->execute();
      $results = $records->fetch(PDO::FETCH_ASSOC);

      $message = '';

      if (count($results) > 0 && password_verify($_POST['password'], $results['password'])) {
        $_SESSION['user_id'] = $results['id'];
        header("Location: /php-login");
      } else {
        $message = 'Sorry, those credentials do not match';
      }
    }

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Iniciar sesion</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="assets/style.css">
  </head>
  <body>
    <?php require 'partials/header.php' ?>

    <?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>

    <h1>Iniciar sesion</h1>
    <span>o <a href="signup.php">Registrarse</a></span>

    <form action="datos.php" method="POST">
      <input name="Email "type="text" placeholder="Ingrese su Email" required=""> 
      <input name="Contrasena" type="password" placeholder="Ingrese su Contrasena" required="">
      <input type="Submit" value="Ingresar">
    </form>
  </body>
</html>
